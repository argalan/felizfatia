import React from 'react';
import { Switch, Route, Router, Redirect} from 'react-router-dom';
import Main from './pages/Main';
import History from './history';
import {useSelector} from 'react-redux';

const isLoggedIn = (u) => {
  if(localStorage.getItem('U_ID') && u === 1){
    return true;
  }else{
    return false;
  }
}
  
  const SecuredRoute = ({ component: Component, userType:user, ...rest }) => (
      
    <Route
      {...rest}
      render={props =>
      
        isLoggedIn(user) === true ? (
          <Component {...props} />
        ) : (
          <Redirect to="/" />
        )

      }
    />
  );

  // const SecuredLogin = ({ component: Component, ...rest})=>(
  //     <Route {...rest} render={props => }
  // );
  

const Routes = ()=>{
  const user = useSelector(state => state.usuario.user);
  return <>
    <Router history={History}>
        <Switch>
            <Route exact path="/" component={Main}/>
      


          
        </Switch>
    </Router>
    </>
};

export default Routes;